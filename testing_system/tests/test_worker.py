# encoding: utf-8
from unittest import mock

from requests import Response
import pytest

from worker import exceptions, extensions


def test_worker_test_solution_full(current_app, hello_world_problem):
    report = current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['root'],
            testing_mode='full'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 6
    assert report_lines[3] == ''

    for report_line_id, report_line in enumerate(report_lines[:3], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


def test_worker_test_solution_first_fail(current_app, hello_world_problem):
    report = current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['root'],
            testing_mode='first_fail'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 6
    assert report_lines[3] == ''

    for report_line_id, report_line in enumerate(report_lines[:3], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


def test_worker_test_solution_one(current_app, hello_world_problem):
    report = current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['root'],
            testing_mode='one'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 4
    assert report_lines[1] == ''

    for report_line_id, report_line in enumerate(report_lines[:1], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


@mock.patch.object(extensions.dots_api, 'authenticate')
def test_worker_run_worker_loop(
        mocked_dots_api_authenticate,
        current_app
    ):
    new_solution = {
            'solution_name': '1_1000.1001.01F',
            'solution_id': '1',
            'problem_id': '1000',
            'user_id': '1001',
            'programming_language_id': '01',
            'testing_mode': 'full',
            'solution_source': 'XXX',
            'problem_path': '/tmp/problems/1000',
        }
    report = '1 OK 100 0 0\n\n'

    def fake_dots_api_get(url, data=None):
        call_count = getattr(fake_dots_api_get, 'call_count', 0)
        fake_dots_api_get.call_count = call_count + 1
        response = Response()
        if url == '/lock/%s' % new_solution['solution_name']:
            response.status_code = 200
            response._content = b''
        elif url == '/result/%s' % new_solution['solution_name']:
            assert data == report.encode('utf-8')
            response.status_code = 200
            response._content = b'1 OK'
        return response

    def fake_get_solution():
        call_count = getattr(fake_get_solution, 'call_count', 0)
        fake_get_solution.call_count = call_count + 1
        if call_count == 0:
            return new_solution
        if call_count == 1:
            raise exceptions.TestingSystemRestartSignal()

    with mock.patch.object(extensions.dots_api, 'get', fake_dots_api_get) as mocked_dots_api_get:
        with mock.patch.object(extensions.solutions_queue_manager, 'start'):
            with mock.patch.object(
                    extensions.solutions_queue_manager,
                    'get_solution',
                    fake_get_solution
                ):
                with mock.patch.object(current_app, 'test_solution', return_value=report):
                    with pytest.raises(exceptions.TestingSystemRestartSignal):
                        current_app.run_worker_loop()

    assert mocked_dots_api_get.call_count == 2
