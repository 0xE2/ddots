# encoding: utf-8
import pytest

from worker.extensions import runners


class App(object):
    class config(object):
        DOCKER_DAEMON_API_URL = 'unix:///var/run/docker.sock'
        CONTAINER_MEMORY_HARD_LIMIT = 512 * 1024 * 1024
        RUNNER_STACK_HARD_LIMIT = 64 * 1024 * 1024
        BASE_CONTAINER_NAME = 'runners_testing'
        SHARED_VOLUMES = {
                'data': {
                        'volume_name': 'ddots-testing-system-testing-data',
                        'mount_point': '/data',
                    },
                'sandbox': {
                        'volume_name': 'ddots-testing-system-testing-sandbox',
                        'mount_point': '/sandbox',
                    },
            }
        CPU_ID = 0
        RUNNER_EXTRA_OPTIONS = {}
        RUNNER_TIME_LIMIT_FACTOR = 0.5
        DOTS_LANG_ID_TO_RUNNER = {}


@pytest.yield_fixture(scope='session')
def runners_manager():
    runners_manager = runners.RunnersManager(app=App())
    runners_manager.init_containers_list(['bash'])
    yield runners_manager
    runners_manager.rm_container('bash')


def test_RunnersManager(runners_manager, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.sh')
    OK_solution_path.write(
            'echo "Hello World!" > output.txt\n'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(OK_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 3, report
    for report_line in report_lines:
        parsed_report_line = report_line.split()
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert report_status == 'OK'
        assert float(points) > 0


def test_RunnersManager_wrong_answer(runners_manager, tmpdir, hello_world_problem):
    WA_solution_path = tmpdir.join('WA_solution.sh')
    WA_solution_path.write(
            'echo "Hello World2!" > output.txt\n'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(WA_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 2, report
    for report_line in report_lines:
        parsed_report_line = report_line.split()
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert report_status == 'WA'
        assert float(points) == 0


def test_RunnersManager_presentation_error_empty_output(
        runners_manager, tmpdir, hello_world_problem
    ):
    PE_solution_path = tmpdir.join('PE_solution.sh')
    PE_solution_path.write(
            'echo "" > output.txt\n'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(PE_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 2, report
    for report_line in report_lines:
        parsed_report_line = report_line.split()
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert report_status == 'PE'
        assert float(points) == 0


def test_RunnersManager_ignore_stderr(
        runners_manager, tmpdir, hello_world_problem
    ):
    OK_solution_path = tmpdir.join('OK_solution.sh')
    OK_solution_path.write(
            'echo "Hello World!" > output.txt\n'
            'echo "IGNORE THIS!" >&2'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(OK_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 3, report
    for report_line in report_lines:
        parsed_report_line = report_line.split()
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert report_status == 'OK'
        assert float(points) > 0


def test_RunnersManager_presentation_error_missing_output(
        runners_manager, tmpdir, hello_world_problem
    ):
    PE_solution_path = tmpdir.join('PE_solution.sh')
    PE_solution_path.write(
            'exit 0\n'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(PE_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 2, report
    for report_line in report_lines:
        parsed_report_line = report_line.split()
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert report_status == 'PE'
        assert float(points) == 0


def test_RunnersManager_stdin_stdout(runners_manager, tmpdir, hello_world_problem):
    stdin_stdout_solution_path = tmpdir.join('stdin_stdout_solution.sh')
    stdin_stdout_solution_path.write(
            'echo "Hello $(cat)"\n'
        )
    report = runners_manager.start(
            'bash',
            problem_path=hello_world_problem['root'],
            testing_mode=runners.RunnersManager.TM_FULL,
            solution_executable_path=str(stdin_stdout_solution_path),
        )
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 2, report

    parsed_report_line = report_lines[0].split()
    assert len(parsed_report_line) == 5, report_lines[0]
    test_id, report_status, points, _, _ = parsed_report_line
    if hello_world_problem['output_file'] == 'stdout':
        assert report_status == 'WA'
    else:
        assert report_status == 'PE'
    assert float(points) == 0

    parsed_report_line = report_lines[1].split()
    assert len(parsed_report_line) == 5, report_lines[1]
    test_id, report_status, points, _, _ = parsed_report_line
    if hello_world_problem['output_file'] == 'stdout':
        if hello_world_problem['input_file'] == 'stdin':
            assert report_status == 'OK'
            assert float(points) > 0
        else:
            assert report_status == 'WA'
            assert float(points) == 0
    else:
        assert report_status == 'PE'
        assert float(points) == 0
