import queue
import threading
from unittest import mock

import pytest
from requests import Response

from worker.extensions import dots_api, problems_manager, solutions


def test_SolutionsQueuePrefetchThread_check_new_solution(current_app):
    solutions_queue_prefetch_thread = solutions.SolutionsQueuePrefetchThread(
            None,
            None,
            supported_programming_language_ids={'01'},
            delay_between_checks=0.5
        )

    def fake_dots_api_get(url):
        response = Response()
        if url == '/solution/':
            response.status_code = 200
            response._content = b'1_1000.1001.01F'
        elif url == '/solution/1_1000.1001.01F':
            response.status_code = 200
            response._content = b'echo "Hello World!" > output.txt'
        return response

    with mock.patch.object(dots_api, 'get', fake_dots_api_get) as dots_api_get:
        with mock.patch.object(
                problems_manager,
                'prepare_problem',
                return_value='/tmp/ddots/problems/1000'
            ) as problems_manager_prepare_problem:
            new_solution = solutions_queue_prefetch_thread._check_new_solution()

    assert isinstance(new_solution, dict)
    assert set(new_solution.keys()) >= {
            'problem_id',
            'problem_path',
            'solution_id',
            'solution_name',
            'solution_source',
            'programming_language_id',
            'testing_mode_code',
        }


def test_SolutionsQueuePrefetchThread_run(current_app):
    break_event = threading.Event()
    new_solutions_queue = queue.Queue()
    solutions_queue_prefetch_thread = solutions.SolutionsQueuePrefetchThread(
            new_solutions_queue,
            break_event,
            supported_programming_language_ids={'01'},
            delay_between_checks=0.5
        )

    def fake_dots_api_get(url):
        response = Response()
        if url == '/solution/':
            response.status_code = 200
            response._content = b'0 no solutions'
        else:
            assert False
        return response

    def stop_solution_queue_fetching():
        import time
        time.sleep(0.5)
        break_event.set()

    stop_thread = threading.Thread(target=stop_solution_queue_fetching)
    with mock.patch.object(dots_api, 'get', fake_dots_api_get) as dots_api_get:
        with mock.patch.object(
                problems_manager,
                'prepare_problem',
                return_value='/tmp/ddots/problems/1000'
            ) as problems_manager_prepare_problem:
            stop_thread.start()
            solutions_queue_prefetch_thread.run()

    stop_thread.join()
    assert isinstance(new_solutions_queue.get_nowait(), StopIteration)


def test_SolutionsQueueManager():
    class App(object):
        class config(object):
            PREFETCH_SOLUTIONS_COUNT = 2
            DOTS_LANG_ID_TO_RUNNER = {
                    '01': 'bash',
                }
            PREFETCH_DELAY_BETWEEN_CHECKS = 0.5

    solutions_queue_manager = solutions.SolutionsQueueManager(app=App())
    new_solution = {'solution_id': 'xx'}
    solutions_queue_manager._new_solutions_queue.put(new_solution)
    assert solutions_queue_manager.get_solution() == new_solution
