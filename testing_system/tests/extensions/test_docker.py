# encoding: utf-8
from worker.extensions import docker


class App(object):
    class config(object):
        DOCKER_DAEMON_API_URL = 'unix:///var/run/docker.sock'
        CONTAINER_MEMORY_HARD_LIMIT = 256 * 1024 * 1024
        SHARED_VOLUMES = {
                'data': {
                        'volume_name': 'ddots-testing-system-testing-data',
                        'mount_point': '/data',
                    },
                'sandbox': {
                        'volume_name': 'ddots-testing-system-testing-sandbox',
                        'mount_point': '/sandbox',
                    },
            }


def test_DockerManager():
    class DockerManager(docker.DockerManager):
        def _get_container_name(self, container_suffix):
            return 'ddots-' + container_suffix

        def _get_container_init_options(self, *args, **kwargs):
            container_init_options = super(DockerManager, self)\
                ._get_container_init_options(*args, **kwargs)
            container_init_options.update({
                    'entrypoint': '/bin/sh',
                    'command': ['-c', 'echo "STDOUT" && echo "STDERR" >&2'],
                })
            return container_init_options

    docker_manager = DockerManager(app=App())

    return
    docker_manager.init_container('runner-binary')
    exit_code, outputs = docker_manager.start('runner-binary')
    assert exit_code == 0
    assert outputs == ("STDOUT\n", "STDERR\n")
    docker_manager.kill_container('runner-binary')
    docker_manager.rm_container('runner-binary')
