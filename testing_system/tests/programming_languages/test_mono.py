# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_mono_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.cs')
    OK_solution_path.write(
            'using System.IO;'
            'class Program {'
            '    static void Main(string[] args) {'
            '        StreamWriter sw = new StreamWriter("output.txt");'
            '        sw.Write("Hello World!");'
            '        sw.Close();'
            '    }'
            '}'
        )
    testing_report = compiler_runner(
            'mono',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_mono_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.cs')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'mono',
                problem_path=hello_world_problem['root'],
                solution_source_path=str(CE_solution_path)
            )
    assert 'solution.source(1,0): error CS1525: Unexpected symbol `if\'' in str(CE.value)
