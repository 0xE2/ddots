# encoding: utf-8
import pytest

def test_jaguar_OK(runner, tmpdir, hello_world_problem):
    if hello_world_problem['output_file'] != 'stdout':
        raise pytest.skip()
    OK_solution_path = tmpdir.join('OK_solution.jag')
    OK_solution_path.write('print: "Hello World!"')
    testing_report = runner(
            'jaguar',
            problem_path=hello_world_problem['root'],
            solution_executable_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']
